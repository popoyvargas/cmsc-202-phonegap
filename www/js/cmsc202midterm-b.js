/** BATTERY STATUS */
function showBatteryStatus()
{
	message = "BATTERY STATUS:<br>Level: " + Math.round( batteryStatus.level ) + "%<br>";
	message += batteryStatus.isPlugged ? "[Charging...]" : "[Not Charging]";
	displayMessage( message );
}

/** GEO LOCATION */
function getGeoLocation()
{
	navigator.geolocation.getCurrentPosition( onSuccess, onError );
}

var onSuccess = function (position )
{
	displayMessage( `Latitude: ${position.coords.latitude}\nLongitude: ${position.coords.longitude}` );
};

function onError(error) {
	displayMessage( `code: ${error.code}\nmessage: ${error.message}` );
}

/** VIBRATE */
function vibrate()
{
	// navigator.vibrate( [ 1000, 1000, 3000, 1000, 5000 ] );
	navigator.vibrate( 2000 );
}

/** CLICK AND SAVE PHOTO */
var options =
{
	quality: 100,
	destinationType: Camera.DestinationType.FILE_URI,
	sourceType: Camera.PictureSourceType.CAMERA,
	encodingType: Camera.EncodingType.JPEG,
	cameraDirection: 1,
	saveToPhotoAlbum: true
};

function takePhoto()
{
	navigator.camera.getPicture( function( imagePath )
	{
		//Grab the file name of the photo in the temporary directory
		var currentName = imagePath.replace( /^.*[\\\/]/, '' );
	
		//Create a new name for the photo
		var d = new Date(),
			n = d.getTime(),
			newFileName = n + ".jpg";
		/**
		//Move the file to permanent storage
		$cordovaFile.moveFile( cordova.file.tempDirectory, currentName, cordova.file.dataDirectory, newFileName )
			.then( function( success )
			{
				//success.nativeURL will contain the path to the photo in permanent storage, do whatever you wish with it, e.g:
				//createPhoto(success.nativeURL);
			}, function( error )
			{
				//an error occured
			});
		
		var myImage = document.getElementById( 'myPhoto' );
		myImage.style.display = 'block';
		myImage.src = "data:image/jpeg;base64," + imageData;
		*/
	}, function( error )
	{
		displayMessage( error );
	}, options );
}

/** DISPLAY MESSAGE */
function displayMessage( message )
{
	document.getElementById( 'messageSpan' ).innerHTML = message;
	document.getElementById( 'messageDiv' ).style.display = 'block';
}
